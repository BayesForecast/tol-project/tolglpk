#define LOCAL_NAMEBLOCK _local_namebtntLock_

#if defined(_MSC_VER)
#  include <win_tolinc.h>
#endif

#include <tol/tol_LoadDynLib.h>
#include <tol/tol_bcommon.h>
#include <tol/tol_boper.h>
#include <tol/tol_bnameblock.h>
#include <tol/tol_btxtgra.h>
#include <tol/tol_bmatgra.h>
#include <tol/tol_bvmatgra.h>
#include <tol/tol_bvmat_impl.h>
#include <tol/tol_bprdist.h>
#include <tol/tol_bprdist_internal.h>
#include <tol/tol_gsl.h>
#include <gsl/gsl_sf.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_cdf.h>
#include <tol/tol_bdat.h>

#include <glpk.h>



#define dMat(arg) ((DMat&)Mat(arg))
#define b2dMat(M) ((DMat&)(M))


#define dMat(arg) ((DMat&)Mat(arg))
#define b2dMat(M) ((DMat&)(M))

//Creates local namebtntLock container
static BUserNameBlock* _local_unb_ = NewUserNameBlock();

//Creates the reference to local namebtntLock
static BNameBlock& _local_namebtntLock_ = _local_unb_->Contens();

//Entry point of library returns the NameBlock to LoadDynLib
//This is the only one exported function
DynAPI void* GetDynLibNameBlockTolGlpk()
{
  BUserNameBlock* copy = NewUserNameBlock();
  copy->Contens() = _local_unb_->Contens();
  return(copy);
}

int TolGlpk_output_handler(void *info, const char *s)
{
  Std( s );
  return 1;
}

#define DEBUG_ARGS

class BaseMatrixAccessor
{
public:
  BaseMatrixAccessor() {}
  virtual ~BaseMatrixAccessor() {}
  virtual int Rows() = 0;
  virtual int Columns() = 0;
  virtual double operator()( int i, int j ) = 0;
  virtual long GetNonZeroSize() = 0;
  virtual bool GetAsTriplet( int * ptrI, int* ptrJ, double* ptrX ) = 0;
};

class BMatAccessor : public BaseMatrixAccessor
{
public:

  BMatAccessor( DMat& target )
    : m_target( target ) {}

  virtual ~BMatAccessor() {}
  virtual int Rows();
  virtual int Columns();
  virtual double operator()( int i, int j );
  virtual long GetNonZeroSize();
  virtual bool GetAsTriplet( int * ptrI, int * ptrJ, double * ptrX );

private:
  DMat& m_target;
};

class BVMatAccessor : public BaseMatrixAccessor
{
public:

  BVMatAccessor( BVMat& target )
    : m_target( target ) {}
  virtual ~BVMatAccessor() {}
  virtual int Rows();
  virtual int Columns();
  virtual double operator()( int i, int j );
  virtual long GetNonZeroSize();
  virtual bool GetAsTriplet( int * ptrI, int * ptrJ, double * ptrX );

private:
  BVMat& m_target;
};

// Implementation: BMatAccessor

int BMatAccessor::Rows()
{
  return this->m_target.Rows();
}

int BMatAccessor::Columns()
{
  return this->m_target.Columns();
}

double BMatAccessor::operator()( int i, int j )
{
  return this->m_target( i, j );
}

long BMatAccessor::GetNonZeroSize()
{
  return this->m_target.Rows() * this->m_target.Columns();
}

bool BMatAccessor::GetAsTriplet( int* ptrI, int* ptrJ, double* ptrX )
{
  int nR = this->m_target.Rows();
  int nC = this->m_target.Columns();

  long o;
  for ( int i = 0; i < nR; i++ )
    {
    for ( int j = 0; j < nC; j++ )
      {
      o = i * nC + j;
      // return the cell indexes as 1-based, the same convention used
      // at tol user level which is the same returned by
      // BVMat::GetTriplet
      ptrI[ o ] = i+1;
      ptrJ[ o ] = j+1;
      ptrX[ o ] = this->m_target( i, j );
      }
    }
  return true;
}

// Implementation: BVMatAccessor

int BVMatAccessor::Rows()
{
  return this->m_target.Rows();
}

int BVMatAccessor::Columns()
{
  return this->m_target.Columns();
}

double BVMatAccessor::operator()( int i, int j )
{
  return this->m_target.GetCell( i, j );
}

long BVMatAccessor::GetNonZeroSize()
{
  return this->m_target.NonNullCells();
}

bool BVMatAccessor::GetAsTriplet( int* ptrI, int* ptrJ, double * ptrX )
{
  BVMat T( m_target, BVMat::ESC_chlmRtriplet);
  if( ( T.code_ != BVMat::ESC_chlmRtriplet ) || !T.s_.undefined_ ) 
    {
    return false;
    }
  long n = T.s_.chlmRtriplet_->nnz;
  int*    i = (int*)   T.s_.chlmRtriplet_->i;
  int*    j = (int*)   T.s_.chlmRtriplet_->j;
  double* x = (double*)T.s_.chlmRtriplet_->x;
  // cell index are returned as 1-based because this is the user
  // level convention in TOL and also the expected by Glpk
  for ( long k = 0; k < n; k++ )
    {
    ptrI[ k ] = i[ k ] + 1;
    ptrJ[ k ] = j[ k ] + 1;
    ptrX[ k ] = x[ k ];
    }
  return true;
}

static
BaseMatrixAccessor *BuildMatrix( BSyntaxObject *arg, const char * argName )
{
  BaseMatrixAccessor *m = NULL;
  char msgBuffer[ 512 ];

  if ( arg->Grammar() == GraMatrix() )
    {
    DMat& argM = dMat( arg );
    m = new BMatAccessor( argM );
    }
  else if ( arg->Grammar() == GraVMatrix() )
    {
    BVMat& argM = VMat( arg );
    m = new BVMatAccessor( argM );
    }
  else
    {
    snprintf( msgBuffer, sizeof( msgBuffer ),
	      "wrong type for argumente '%s' must be Matrix or VMatrix",
              argName );
    Error( msgBuffer );
    }
  return m;
}

//--------------------------------------------------------------------
DeclareContensClass(BSet, BSetTemporary, BSetGlpkSolve);
DefMethod(1, BSetGlpkSolve, "solveLP", 6, 7,
  "Anything Anything Anything Anything Anything Anything Set",
  "(Anything C, Anything A, Anything L, Anything U, Anything Lx, Anything Ux [, Set Options])",
  "Documentar!!!",
  BOperClassify::NumericalAnalysis_);

//--------------------------------------------------------------------
void BSetGlpkSolve::CalcContens()
{
  /*
  DMat& C = dMat( Arg( 1 ) );
  DMat& A = dMat( Arg( 2 ) );
  DMat& L = dMat( Arg( 3 ) );
  DMat& U = dMat( Arg( 4 ) );
  DMat& Lx = dMat( Arg( 5 ) );
  DMat& Ux = dMat( Arg( 6 ) );
  */

  BaseMatrixAccessor *C = NULL;
  BaseMatrixAccessor *A = NULL;
  BaseMatrixAccessor *L = NULL;
  BaseMatrixAccessor *U = NULL;
  BaseMatrixAccessor *Lx = NULL;
  BaseMatrixAccessor *Ux = NULL;

#define CLEAN_MATRICES     \
  do                       \
    {                      \
    if ( C ) delete C;     \
    if ( A ) delete A;     \
    if ( L ) delete L;     \
    if ( U ) delete U;     \
    if ( Lx ) delete Lx;   \
    if ( Ux ) delete Ux;   \
    }  while ( 0 );
 

#define BUILD_MATRIX( O, A, N )                   \
  do                                              \
    {                                             \
    O = BuildMatrix( A, N );                      \
    if ( !O )                                     \
      {                                           \
      CLEAN_MATRICES;                             \
      return;                                     \
      }                                           \
    } while ( 0 );

#define RETURN_ON_ERROR( msg )                  \
  do                                            \
    {                                           \
    Error( msg );                               \
    CLEAN_MATRICES;                             \
    return;                                     \
    } while ( 0 );

  BUILD_MATRIX( C, Arg( 1 ), "C" );
  BUILD_MATRIX( A, Arg( 2 ), "A" );
  BUILD_MATRIX( L, Arg( 3 ), "L" );
  BUILD_MATRIX( U, Arg( 4 ), "U" );
  BUILD_MATRIX( Lx, Arg( 5 ), "Lx" );
  BUILD_MATRIX( Ux, Arg( 6 ), "Ux" );

  bool verbose = true;
  bool maximize = false;
  int *typeVars = NULL;
  bool needMIP = false;
  float maxSec = 0;
  long maxIter = -1;

  long nVarsC = C->Rows();
  long nVarsA = A->Columns();
  long nA = A->Rows();
  long nL = L->Rows();
  long nU = U->Rows();
  long nLx = Lx->Rows();
  long nUx = Ux->Rows();

  char msgBuffer[ 512 ];

  if ( nVarsC != nVarsA ) 
    {
    snprintf( msgBuffer, sizeof( msgBuffer ),
	      "Number of variables missmatch between objective function and constraint. Objective function has %ld variables while constraint matrix has %ld", nVarsC, nVarsA );
    RETURN_ON_ERROR( msgBuffer );
    }
  if ( nL != nU ) 
    {
    snprintf( msgBuffer, sizeof( msgBuffer ),
              "Length missmatch between constraint bound. Lower bound L has %ld rows while upper U bound has %ld", nL, nU );
    RETURN_ON_ERROR( msgBuffer );
    }
  if ( nL != nA ) 
    {
    snprintf( msgBuffer, sizeof( msgBuffer ),
	      "Length missmatch between constraint bounds and constraint matrix. Bounds L and U has %ld rows while constraint matrix A has %ld", nL, nA );
    RETURN_ON_ERROR( msgBuffer );
  }
  if ( nLx != nUx ) 
    {
    snprintf( msgBuffer, sizeof( msgBuffer ),
	      "Length missmatch between variable bounds. Lower bound Lx has %ld rows while upper Ux bound has %ld", nLx, nUx );
    RETURN_ON_ERROR( msgBuffer );
  }
  if ( nLx != nVarsC ) 
    {
    snprintf( msgBuffer, sizeof( msgBuffer ),
	      "Number of variables missmatch between objective and variable bounds. Objective function has %ld variables while variable bound Lx and Ux has %ld", nVarsC, nLx );
    RETURN_ON_ERROR( msgBuffer );
    }

  if ( Arg( 7 ) )
    {
#ifdef DEBUG_ARGS
    Std( "Optional argument 'Options' provided\n" );
#endif
    BSet& options = Set( Arg( 7 ) );
    BSyntaxObject *tmpObj = NULL;

    // check for option Verbose
    int idx = options.FindIndexByName( "Verbose" );
    if ( idx ) 
      {
#ifdef DEBUG_ARGS
      Std( "Option 'Verbose' provided\n" );
#endif
      tmpObj = options[ idx ];
      if ( tmpObj->Grammar() == GraReal() ) 
        {
        BDat &datVerbose = Dat( tmpObj );
        verbose = datVerbose.Value() != 0.0 ? true : false;
        } 
      else 
        {
        RETURN_ON_ERROR( "Invalid 'Verbose' option, must be Real, 0 or 1" );
        }
      }

    // check for option maximize
    idx = options.FindIndexByName( "Maximize" );
    if ( idx ) 
      {
#ifdef DEBUG_ARGS
      Std( "Option 'Maximize' provided\n" );
#endif
      tmpObj = options[ idx ];
      if ( tmpObj->Grammar() == GraReal() )
        {
        BDat &datMax = Dat( tmpObj );
	maximize = datMax.Value() != 0.0 ? true : false;
        } 
      else 
        {
        RETURN_ON_ERROR( "Invalid 'Maximize' option, must be Real, 0 or 1" );
        }
      }

    // check for option Types
    idx = options.FindIndexByName( "Types" );
    if ( idx )
      {
#ifdef DEBUG_ARGS
      Std( "Option 'Types' provided\n" );
#endif
      tmpObj = options[ idx ];
      if ( tmpObj->Grammar() == GraSet() )
        {
	BSet &setTypes = Set( tmpObj );

	if ( setTypes.Card() )
          {
	  typeVars = new int[ setTypes.Card() ];
	  for ( long i = 1; i <= setTypes.Card(); i++ )
            {
	    BSyntaxObject *item = setTypes[ i ];
	    if ( item->Grammar() == GraText() )
              {
	      BText &txtItem = Text( item );
	      if ( !strcasecmp( txtItem.Buffer(), "C" ) ) 
                {
		typeVars[ i-1 ] = GLP_CV;
                } 
              else if ( !strcasecmp( txtItem.Buffer(), "I" ) ) 
                {
		typeVars[ i-1 ] = GLP_IV;
		needMIP = true;
                } 
              else if ( !strcasecmp( txtItem.Buffer(), "B" ) ) 
                {
		typeVars[ i-1 ] = GLP_BV;
		needMIP = true;
                } else 
                {
		snprintf( msgBuffer, sizeof( msgBuffer ),
			  "Invalid variable type \"%s\" specified for variable %ld-nth, must be \"C\", \"I\" or \"B\"",
			  txtItem.Buffer(), i );
		delete []typeVars;
                RETURN_ON_ERROR( msgBuffer );
                }
              }
            else 
              {
	      snprintf( msgBuffer, sizeof( msgBuffer ),
			"Invalid object in Types at index %ld-nth, must be \"C\", \"I\" or \"B\"", i );
	      delete []typeVars;
              RETURN_ON_ERROR( msgBuffer );
              }
            }
          } 
        else 
          {
	  Warning( "Provided 'Types' option as an empty Set. Ignoring this option." );
          }
        }
      else 
        {
        RETURN_ON_ERROR( "Invalid 'Types' option, must be Set" );
        }
      }

    // check for option maxSec
    idx = options.FindIndexByName( "maxSec" );
    if ( idx )
      {
#ifdef DEBUG_ARGS
      Std( "Option 'maxSec' provided\n" );
#endif
      tmpObj = options[ idx ];
      if ( tmpObj->Grammar() == GraReal() ) 
        {
	BDat &datMaxSec = Dat( tmpObj );
	maxSec = datMaxSec.Value();
        } 
      else 
        {
	RETURN_ON_ERROR( "Invalid 'maxSec' option, must be Real" );
        }
      }  
      // check for option maxIter
      idx = options.FindIndexByName( "maxIter" );
      if ( idx ) 
        {
#ifdef DEBUG_ARGS
        Std( "Option 'maxIter' provided\n" );
#endif
        tmpObj = options[ idx ];
        if ( tmpObj->Grammar() == GraReal() ) 
          {
          BDat &datMaxIter = Dat( tmpObj );
          maxIter = (long)datMaxIter.Value();
          }
        else 
          {
          RETURN_ON_ERROR( "Invalid 'maxIter' option, must be Real" );
          return;
          }
        }
    }

  int *typeBoundsA = new int[ nL ];

  for ( long i = 0; i < nL; i++ ) 
    {
    BDat l( (*L)(i,0) );
    BDat u( (*U)(i,0) );
    
    if ( l.IsKnown() && u.IsKnown() ) 
      {
      if ( l <= u ) 
        {
	int t = l.IsFinite() + 2 * u.IsFinite();
	if ( ( t == 3 ) && ( l == u ) ) 
          {
	  t = 4;
          }
	typeBoundsA[ i ] = t;
        } 
      else 
        {
	snprintf( msgBuffer, sizeof( msgBuffer ),
		  "Invalid bounds for constraint row %ld-nth, lower bound must be less than upper bound", i );
	Error( msgBuffer );
        }
      }
    else 
      {
      delete []typeVars;
      delete []typeBoundsA;
      snprintf( msgBuffer, sizeof( msgBuffer ),
		"Invalid bound for constraint row %ld-nth, must be known", i );
      RETURN_ON_ERROR( msgBuffer );
      return;
      }
    }

  int *typeBoundsX = new int[ nVarsC ];

  for ( long i = 0; i < nVarsC; i++ ) 
    {
    if ( typeVars && ( typeVars[ i ] == GLP_BV ) ) 
      {
      // set to a valid value, no bound will be added for this variable
      typeBoundsX[ i ] = 5;
      continue;
      }
    BDat l( (*Lx)( i, 0 ) );
    BDat u( (*Ux)( i, 0 ) );

    if ( l.IsKnown() && u.IsKnown() ) 
      {
      if ( l <= u ) 
        {
	int t = l.IsFinite() + 2 * u.IsFinite();
	if ( ( t == 3 ) && ( l == u ) ) 
          {
	  t = 4;
          }
	typeBoundsX[ i ] = t;
      } 
      else 
        {
	snprintf( msgBuffer, sizeof( msgBuffer ),
		  "Invalid bounds for variable %ld-nth, lower bound must be less than upper bound", i );
	Error( msgBuffer );
        // OJO: por qué no se retorna aquí?
        }
      } 
    else
      {
      delete []typeVars;
      delete []typeBoundsA;
      delete []typeBoundsX;
      snprintf( msgBuffer, sizeof( msgBuffer ),
		"Invalid bound for variable %ld-nth, must be known", i );
      RETURN_ON_ERROR( msgBuffer );
      }
    }

#ifdef DEBUG_ARGS
  Std( "All arguments processed and validated" );
#endif

  if ( verbose ) 
    {
    glp_term_hook( TolGlpk_output_handler, NULL );
    } 
  else 
    {
    glp_term_hook( NULL, NULL );
    }
  ///
  glp_prob *lp;
  lp  = glp_create_prob();
  if ( maximize )
    glp_set_obj_dir( lp, GLP_MAX );
  else
    glp_set_obj_dir( lp, GLP_MIN );

  glp_add_rows(lp, nA);
  for (int i = 0; i < nA; i++ ) 
    {
    //Chequear que L<=U
    // int vv = IS_FINITE(L(i,0)) + 2*IS_FINITE(U(i,0));
    switch( typeBoundsA[ i ] ) 
      {
      case 0:
        glp_set_row_bnds( lp, i + 1, GLP_FR, 0.0, 0.0 );
        break;
      case 1:
        glp_set_row_bnds( lp, i + 1, GLP_LO, (*L)( i, 0 ), 0.0 );
        break;
      case 2:
        glp_set_row_bnds( lp, i + 1, GLP_UP, 0.0, (*U)( i, 0 ) );
        break;
      case 3:
        glp_set_row_bnds( lp, i + 1, GLP_DB, (*L)( i, 0 ), (*U)( i, 0 ) );
        break;
      case 4:
        glp_set_row_bnds( lp, i + 1, GLP_FX, (*L)( i, 0 ), 0.0 );
        break;
      }
    }

  glp_add_cols( lp, nVarsA );
  for (int i = 0; i < nVarsA; i++ ) 
    {
    //chequear que Lx<=Ux
    // int vv = IS_FINITE(Lx(i,0)) + 2*IS_FINITE(Ux(i,0));
    switch( typeBoundsX[ i ] ) 
      {
      case 0:
        glp_set_col_bnds( lp, i + 1, GLP_FR, 0.0, 0.0 );
        break;
      case 1:
        glp_set_col_bnds( lp, i+1, GLP_LO, (*Lx)( i, 0 ), 0.0 );
        break;
      case 2:
        glp_set_col_bnds( lp, i + 1, GLP_UP, 0.0, (*Ux)( i, 0 ) );
        break;
      case 3:
        glp_set_col_bnds( lp, i + 1, GLP_DB, (*Lx)( i, 0 ), (*Ux)( i, 0 ) );
        break;
      case 4:
        glp_set_col_bnds( lp, i + 1, GLP_FX, (*Lx)(i,0), 0.0 );
        break;
      case 5:
        // this is a binary variable, ignore bounds
        assert( typeVars[ i ] == GLP_BV );
        break;
      }
    glp_set_obj_coef(lp, i+1, (*C)(i, 0));
    }
  // GLPK access the triplet array as 1-based
  int pMax = A->GetNonZeroSize() + 1;
  //reservar memoria
  int *ia = new int[ pMax ];
  int *ja = new int[ pMax ];
  double *ar = new double[ pMax ];

  if ( !A->GetAsTriplet( ia + 1, ja + 1, ar + 1 ) )
    {
    delete []ia;
    delete []ja;
    delete []ar;
    delete []typeVars;
    delete []typeBoundsA;
    delete []typeBoundsX;
    RETURN_ON_ERROR( "Could not read triplet data from matrix A" );
    }
  glp_load_matrix(lp, pMax-1, ia, ja, ar);

  // we don't need the matrix accessors anymore
  CLEAN_MATRICES;

  glp_smcp param_simplex;
  glp_iocp param_mip;

  if ( needMIP ) 
    {
/*
int msg_lev (default: GLP MSG ALL)
Message level for terminal output:
GLP_MSG_OFF-no output;
GLP_MSG_ERR-error and warning messages only;
GLP_MSG_ON normal output;
GLP_MSG_ALL-full output (including informational messages).

int br_tech (default: GLP BR DTH)
Branching technique option:
GLP_BR_FFV-first fractional variable;
GLP_BR_LFV-last fractional variable;
GLP_BR_MFV-most fractional variable;
GLP_BR_DTH-heuristic by Driebeck and Tomlin.

int bt tech (default: GLP BT BLB)
Backtracking technique option:
GLP_BT_DFS-depth first search;
GLP_BT_BFS-breadth first search;
GLP_BT_BLB-best local bound;
GLP_BT_BPH-best projection heuristic.

int pp tech (default: GLP PP ALL)
Preprocessing technique option:
GLP_PP_NONE-disable preprocessing;
GLP_PP_ROOT-perform preprocessing only on the root level;
GLP_PP_ALL -perform preprocessing on all levels.

int fp heur (default: GLP OFF)
Feasibility pump heuristic option:
GLP_ON -enable applying the feasibility pump heuristic;
GLP_OFF-disable applying the feasibility pump heuristic.

int gmi cuts (default: GLP OFF)
Gomory's mixed integer cut option:
GLP_ON -enable generating Gomory's cuts;
GLP_OFF-disable generating Gomory's cuts.

int mir_cuts (default: GLP OFF)
Mixed integer rounding (MIR) cut option:
GLP_ON -enable generating MIR cuts;
GLP_OFF-disable generating MIR cuts.

int cov_cuts (default: GLP OFF)
Mixed cover cut option:
GLP_ON -enable generating mixed cover cuts;
GLP_OFF-disable generating mixed cover cuts.

int clq_cuts (default: GLP OFF)
Clique cut option:
GLP_ON -enable generating clique cuts;
GLP_OFF-disable generating clique cuts.

double tol_int (default: 1e-5)
Absolute tolerance used to check if optimal solution to the current LP
relaxation is integer feasible. (Do not change this parameter without
detailed understanding its purpose.)

double tol_obj (default: 1e-7)
Relative tolerance used to check if the objective value in optimal solution
to the current LP relaxation is not better than in the best known integer
feasible solution. (Do not change this parameter without detailed
understanding its purpose.)

double mip_gap (default: 0.0)
The relative mip gap tolerance. If the relative mip gap for currently
known best integer feasible solution falls below this tolerance, the solver
terminates the search. This allows obtainig suboptimal integer feasible
solutions if solving the problem to optimality takes too long time.

int tm_lim (default: INT MAX)
Searching time limit, in milliseconds.

int out_frq (default: 5000)
Output frequency, in milliseconds. This parameter specifies how frequently
the solver sends information about the solution process to the
terminal.

int out_dly (default: 10000)
Output delay, in milliseconds. This parameter specifies how long the
solver should delay sending information about solution of the current
LP relaxation with the simplex method to the terminal.
void (*cb func)(glp tree *tree, void *info) (default: NULL)
Entry point to the user-defined callback routine. NULL means the advanced
solver interface is not used. For more details see Chapter
"Branch-and-Cut API Routines".

void *cb info (default: NULL)
Transit pointer passed to the routine cb_func (see above).
int cb size (default: 0)
The number of extra (up to 256) bytes allocated for each node of the
branch-and-bound tree to store application-specific data. On creating a
node these bytes are initialized by binary zeros.

int presolve (default: GLP_OFF)
MIP presolver option:
GLP_ON -enable using the MIP presolver;
GLP_OFF-disable using the MIP presolver.

int binarize (default: GLP OFF)
Binarization option (used only if the presolver is enabled):
GLP_ON -replace general integer variables by binary ones;
GLP_OFF-do not use binarization.
    */

    for ( long i = 0; i < nVarsA; i++ ) 
      {
      glp_set_col_kind( lp, i + 1, typeVars[i] );
      }
    
    glp_init_smcp( &param_simplex );
    if ( maxSec > 0 ) 
      {
      param_simplex.tm_lim = maxSec*1000;
      }
    glp_simplex(lp, &param_simplex );

    glp_init_iocp(&param_mip );

    if ( maxSec > 0 ) 
      {
      //param_mip.tm_lim = maxSec*1000;
      param_mip.tm_lim = maxSec*1000;
      }
    if ( maxIter > 0 ) 
      {
      Warning( "MIP optimization does not have stop at given iteration" );
      // param_mip.it_lim = maxIter;
      }
    //param_mip.presolve = GLP_ON;
    if ( maxSec > 0 ) 
      {
      glp_intopt( lp, &param_mip
#ifdef WIN32
		  ,maxSec*1000
#endif
                );
      } 
    else 
      {
      glp_intopt( lp, &param_mip
#ifdef WIN32
		  ,INT_MAX
#endif
		  );
      }

    } 
  else 
    {
    glp_init_smcp( &param_simplex );
    if ( maxSec > 0 ) 
      {
      param_simplex.tm_lim = maxSec*1000;
      }
    if ( maxIter > 0 ) 
      {
      param_simplex.it_lim = maxIter;
      };
    glp_simplex( lp, &param_simplex );
    }

  BMat solution( nVarsC, 1 );
  for( int i=0; i < nVarsC; i++) 
    {
    solution( i, 0 ) =
      needMIP ? glp_mip_col_val( lp, i + 1 ) : glp_get_col_prim( lp, i + 1 );
    }
  BUserMat* matSolution = BContensMat::New("", solution, "GLPK solution");
  matSolution->PutName( "Solution" );

  double objValue = needMIP ?  glp_mip_obj_val( lp ) : glp_get_obj_val( lp );
  BUserDat* datObjValue = BContensDat::New("", objValue, "GLPK objetive found");
  datObjValue->PutName( "Objective" );

  delete []typeVars;
  delete []typeBoundsA;
  delete []typeBoundsX;
  glp_delete_prob( lp );
  delete []ia;
  delete []ja;
  delete []ar;

  BList* lst  = NCons( matSolution );
  contens_.RobElement( Cons( datObjValue, lst ) );
}
